document.addEventListener('submit', function(e) {
  e.preventDefault();
  
  const inputValue = document.getElementById('form_voucher').value;


  	const xhr = new XMLHttpRequest;


  	xhr.open('GET', 'ajaxaction?voucher='+inputValue, true);

  	xhr.onload = function(){

  		if(this.status === 200){

  			const discount = JSON.parse(this.responseText);
  			if( discount != false){

  				//populate id with voucher details
  				document.getElementById('voucherDetails').innerHTML = 
  						'Voucher Name: ' + discount.name +
  						'<br> Voucher discount: ' + discount.discount;

  				const prices = document.querySelectorAll('.planPrice');

  				for(i = 0; i <= prices.length; i++){
  					
  					const amount = prices[i].querySelector('.amount').innerHTML;
  					const newPrice = prices[i].cloneNode(true);
   					//reduce font for old price
  					prices[i].classList.add('strike', 'fontSizeReduce');

  					//get the discounted amount
  					
  					const discountAmount = (amount * discount.discountValue) - amount;
  					newPrice.firstChild.textContent = amount - discountAmount;

  					//append new price HTML element
  					prices[i].parentNode.insertBefore(newPrice, prices[i].nextSibling);

  				}

  			} else {

  				console.log('There is no match or the voucher is already applied');
  			}
  		}
  	}

  	xhr.send();
});