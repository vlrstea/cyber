<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route; 
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Utils\clientInfo;

class ConfPageController extends Controller{

	//Just render the page
	/**
	 * @Route("/conf");
	 */
	public function index( clientInfo $clientInfo, Request $request ){

		$location = $clientInfo->getLocationBasedOnIp($request->getClientIp());

		$clientLocale = $request->getLocale();
		$userAgent = get_browser($request->headers->get('User-Agent'), true);
		$clientIp = $request->getClientIp();
		$vouchersJSON = $clientInfo->getJSON('../var/storage/vouchers.json');
		$appsJSON = $clientInfo->getJSON('../var/storage/app.json');

		return $this->render('confpage.html.twig', array(

			'clientLocale' => $clientLocale,
			'userAgent' => $userAgent,
			'vouchersJSON' => $vouchersJSON,
			'appsJSON' => $appsJSON,
			'location' => $location,

		));
	}
}