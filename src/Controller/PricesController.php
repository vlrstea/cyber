<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route; 
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Utils\Vouchers;
use App\Utils\clientInfo;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

class PricesController extends Controller{

	/**
	 * @Route("/prices", name="prices")
	 */
	public function prices( Request $request ){
		
		$clientInfo = new clientInfo;
		$SO = $clientInfo->getSO( $request->headers->get('User-Agent') );
		$device = 'desktop';

		$json = $clientInfo->getJson('../var/storage/app.json');
		$decodedJSON = json_decode($json, true);
		$theapp = $decodedJSON[$SO]['device'][$device]['plan'];
		
		return $this->render('prices.html.twig', array(

			'SO' => $SO,
			'plans' => $theapp
		));
	}

	/**
	 * @Route("/ajaxaction", name="ajaxaction")
	 */
	public function ajaxAction( Request $request, Vouchers $voucher )
	{

		$data = $request->query->get('voucher');


		//verify if the voucher is not already applied
		if( $request->cookies->has('voucher') && $request->cookies->get('voucher') != $data ){

			//check if there is any voucher with data provided
			$responseToAjax = $voucher->checkVouchers($data);


			if($responseToAjax){
				//set cookie with the (new)voucher
				$response = new Response(json_encode($responseToAjax));
				$response->headers->setCookie(

					new Cookie(

						'voucher', 
						$data, 
						time() + 35000 )

				);

				$response->send();
				//$response->headers->set('Content-Type', 'application/json');

			} 
		}

		return new Response('false');

	}

	public function voucherInput(){

		$form = $this->createFormBuilder([])
		->add('voucher', TextType::class)
		->add('apply', SubmitType::class)
		->getForm();

		return $this->render('voucherInput.html.twig', [

			'form' => $form->createView()

		]);
	}
}