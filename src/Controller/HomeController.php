<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route; 
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Utils\cookiesToSet;
use App\Utils\dummyCookies;
use Symfony\Component\HttpFoundation\Cookie;

class HomeController extends Controller{

	//Just render the page
	/**
	 * @Route("/");
	 */
	public function index( Request $request ){
		
		$setTheCookies = new cookiesToSet;
		$setTheCookies->myCookies();
		
		// $dummy = new dummyCookies;
		// $dummy->setCookie(

		// 	array('voucher' => array(

		// 		'name' => 'voucher',
		// 		'value' => 'notempty',
		// 		'expiry' => 5000

		// 	));
		
		return $this->render('homepage.html.twig');
	}
}