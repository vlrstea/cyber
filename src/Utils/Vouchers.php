<?php
namespace App\Utils;

class Vouchers
{
    public function checkVouchers( $valueToCheck )
    {	

    	//get the vouchers list
        $json = file_get_contents('../var/storage/vouchers.json');
		$json = json_decode($json, true);
		$vouchers = $json['vouchers'];



		//loop through vouchers and check if the passed value matches any vouchers
		foreach($vouchers as $key => $voucher){

			if($voucher['code'] === $valueToCheck){

				return $voucher;
			} 
		}

		return false;
    }
}