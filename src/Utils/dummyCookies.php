<?php
namespace App\Utils;

use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class dummyCookies{

	public function setCookie( array $cookies ){
		
		$response = new Response;
		$request = new Request;

		//loop through cookies array variable(which contains a list with cookies to set)
		foreach($cookies as $key => $value){

			//check if the cookie is already set
			if( !$request->cookies->has( $key ) || $key === 'voucher' ){

				$response->headers->setCookie(

					new Cookie(

						$value['name'], 
						$value['value'], 
						time() + $value['expiry'] )
				);
			}
		}

		return $response->sendHeaders();
	} 

}