<?php
namespace App\Utils;
use App\Utils\dummyCookies;

class cookiesToSet{


	public function myCookies(){

		//array with cookies data
		$array = array(
			'source' => array(

				'name' => 'source',
				'value' => 'website',
				'expiry' => 5000
			),

			'campaign' => array(

				'name' => 'campaign',
				'value' => 'test',
				'expiry' => 5000
			),

			'voucher' => array(

				'name' => 'voucher',
				'value' => 'empty',
				'expiry' => 5000
			),
			
			//test rewriting
			// 'voucher' => array(

			// 	'name' => 'voucher',
			// 	'value' => 'notempty',
			// 	'expiry' => 5000
			// )
		);

		//call the "cookie" setter
		$setCookies = new dummyCookies();
		return $setCookies->setCookie($array);
	}
}