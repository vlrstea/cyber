<?php
namespace App\Utils;

class clientInfo

{	


	public function getJSON( $path = null ){

		if($path){

			$json = file_get_contents( $path );

			return $json;
		}

		return false;
	}

	public function getSO( $userAgent ){

		$SO = ( (preg_match('/windows/i', $userAgent)) ? 'windows' : ((preg_match('/linux|ubuntu/i', $userAgent)) ? 'linux' : 'windows') );


		return $SO;
	}


	//Curl request for user location
	public function getLocationBasedOnIp( $clientIp = null){

		$accessKey = 'c0f0eaec20b5160acfa30cddaeca73a6';


		//init curl
		$ch = curl_init('http://api.ipstack.com/' . $clientIp . '?access_key=' . $accessKey);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		//returned data
		$responseJSON = curl_exec($ch);
		curl_close($ch);

		// decode JSON response
		$response = json_decode($responseJSON, true);

		return $response;
	}

}